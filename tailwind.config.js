/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        lilac: "#706897",
        "light-green": "#B4CFB0",
        "leaf-green": "#B4CFB0",
        "dark-green": "#6D8B74",
        "white-rgba": "rgba(254,254,254,0.95)",
      },
      gridTemplateColumns: {
        16: "repeat(16, minmax(0, 1fr))",
      },
    },
  },
  plugins: [],
};
