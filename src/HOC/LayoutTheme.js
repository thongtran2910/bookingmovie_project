import React from "react";
import Header from "../components/Header";

export default function LayoutTheme({ Component }) {
  return (
    <div
      style={{ minHeight: "100vh", display: "flex", flexDirection: "column" }}
    >
      <Header />
      <div className="flex-grow">
        <Component />
      </div>
    </div>
  );
}
