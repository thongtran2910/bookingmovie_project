import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";
import { Dropdown, Menu, Space } from "antd";

export default function HeaderThemeMobile() {
  const menu = (
    <Menu
      items={[
        {
          label: (
            <NavLink
              to="/showtime"
              className=" mr-5 font-medium hover:text-orange-500 transition duration-300 text-base"
            >
              Lịch chiếu
            </NavLink>
          ),
          key: "0",
        },
        {
          label: (
            <NavLink
              to="/#movie-list"
              className=" mr-5 font-medium hover:text-orange-500 transition duration-300 text-base"
            >
              Danh sách phim
            </NavLink>
          ),
          key: "1",
        },

        {
          label: (
            <NavLink
              to=""
              className=" mr-5 font-medium hover:text-orange-500 transition duration-300 text-base"
            >
              Tin tức
            </NavLink>
          ),
          key: "2",
        },
      ]}
    />
  );
  return (
    <div>
      <div className="w-full bg-white-rgba">
        <div className="h-max py-1 px-2 flex justify-between items-center border-t-2">
          <div className="">
            <Dropdown overlay={menu} trigger={["click"]}>
              <a onClick={(e) => e.preventDefault()}>
                <Space>
                  <span className="font-medium hover:text-red-700 text-black transition duration-300">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      class="h-5 w-5"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                        clip-rule="evenodd"
                      />
                    </svg>
                  </span>
                </Space>
              </a>
            </Dropdown>
          </div>

          <div className="">
            <UserNav />
          </div>
        </div>
      </div>
    </div>
  );
}
