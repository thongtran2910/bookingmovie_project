import React from "react";
import { useSelector } from "react-redux";
import { PuffLoader } from "react-spinners";

export default function SpinnerComponent() {
  const { isLoading } = useSelector((state) => state.spinnerSlice);
  return isLoading ? (
    <div className="fixed w-screen h-screen bg-slate-500 z-50 flex items-center justify-center">
      <PuffLoader color="#706897" size="100" />
    </div>
  ) : (
    <></>
  );
}
