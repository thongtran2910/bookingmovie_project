import React from "react";
import { useWindowSize } from "../Hook/useWindowSize";
import HeaderTheme from "./HeaderTheme";
import HeaderThemeMobile from "./HeaderThemeMobile";

export default function Header() {
  let windowSize = useWindowSize();
  const renderHeader = () => {
    {
      if (windowSize.width > 768) {
        return <HeaderTheme />;
      } else {
        return <HeaderThemeMobile />;
      }
    }
  };
  return <div>{renderHeader()}</div>;
}
