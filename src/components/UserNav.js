import { Dropdown, Menu } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { setUserInfoAction } from "../redux/actions/userAction";
import { localStorageService } from "../services/localStorageService";

export default function UserNav() {
  let userInfo = useSelector((state) => state.userReducer.userInfo);
  let dispatch = useDispatch();
  let handleLogout = () => {
    dispatch(setUserInfoAction(null));
    localStorageService.removeUserInfo();
    window.location.href = "/login";
  };
  let handleSignin = () => {
    window.location.href = "/login";
  };
  const menu = (
    <Menu
      items={[
        {
          key: "1",
          label: (
            <a rel="noopener noreferrer" href="/user">
              Thông tin cá nhân
            </a>
          ),
        },
        {
          key: "2",
          label: (
            <a rel="noopener noreferrer" href="/history">
              Lịch sử đặt vé
            </a>
          ),
        },
      ]}
    />
  );

  return (
    <div>
      {userInfo ? (
        <div className="space-x-4">
          <Dropdown
            overlay={menu}
            placement="bottomLeft"
            className="text-xl text-lilac hover:text-purple-800 font-medium"
          >
            <button>{userInfo?.hoTen}</button>
          </Dropdown>
          <button onClick={handleLogout} className="text-red-500">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-6 w-6"
              fill="none"
              viewBox="0 -3 24 24"
              stroke="currentColor"
              stroke-width="2"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"
              />
            </svg>
          </button>
        </div>
      ) : (
        <div>
          <button
            onClick={handleSignin}
            className="text-gray-400 flex items-center space-x-2 hover:text-lilac"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              class="h-5 w-5"
              viewBox="0 0 20 20"
              fill="currentColor"
            >
              <path
                fill-rule="evenodd"
                d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                clip-rule="evenodd"
              />
            </svg>
            <span>Đăng nhập</span>
          </button>
        </div>
      )}
    </div>
  );
}
