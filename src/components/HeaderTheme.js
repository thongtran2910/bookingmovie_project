import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";
import Lottie from "lottie-react";
import logoAnimate from ".././assets/logo.json";
import "./headerTheme.css";

export default function HeaderTheme() {
  return (
    <div className="h-24 w-full flex items-center justify-between shadow-lg px-20 fixed z-50 bg-white-rgba">
      <NavLink to="/">
        <div className="max-w-[100px]">
          <Lottie animationData={logoAnimate} loop={true} />
        </div>
      </NavLink>
      <div className="space-x-5 text-lg font-medium text-gray-600">
        <a id="tin-tuc" className="hover:text-lilac relative">
          Tin tức
        </a>
        <a
          id="lich-chieu"
          className="hover:text-lilac relative"
          href="/showtime"
        >
          Lịch chiếu
        </a>
        <a id="phim" className="hover:text-lilac relative" href="/#movie-list">
          Phim
        </a>
      </div>
      <UserNav />
    </div>
  );
}
