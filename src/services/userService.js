import axios from "axios";
import { BASE_URL, httpService, TOKEN_CYBERSOFT } from "./configURL";

export const userService = {
  postDangNhap: (dataLogin) => {
    return axios({
      method: "POST",
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      data: dataLogin,
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    });
  },
  postDangKy: (dataSignup) => {
    return axios({
      method: "POST",
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
      data: dataSignup,
      headers: {
        TokenCybersoft: TOKEN_CYBERSOFT,
      },
    });
  },
  postUserInfo: () => {
    return httpService.post("/api/QuanLyNguoiDung/ThongTinTaiKhoan");
  },
  putUpdateUserInfo: (userInfo) => {
    return httpService.put(
      "/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung",
      userInfo
    );
  },
};
