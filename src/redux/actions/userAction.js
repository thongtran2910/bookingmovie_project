import { localStorageService } from "../../services/localStorageService";
import { userService } from "../../services/userService";
import { GET_USER_INFO, SET_USER_INFO } from "../constants/userConstant";

export const setUserInfoAction = (user) => {
  return {
    type: SET_USER_INFO,
    payload: user,
  };
};

export const getUserInfoAction = (userRegist) => {
  return {
    type: GET_USER_INFO,
    payload: userRegist,
  };
};

export const setUserInfoActionService = (
  dataLogin = {},
  handleSuccess = () => {},
  handleFail = () => {}
) => {
  return (dispatch) => {
    userService
      .postDangNhap(dataLogin)
      .then((res) => {
        handleSuccess();
        localStorageService.setUserInfo(res.data.content);
        dispatch({
          type: SET_USER_INFO,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        handleFail();
      });
  };
};

export const getUserInfoActionService = (
  dataSignup = {},
  handleSuccess = () => {},
  handleFail = () => {}
) => {
  return (dispatch) => {
    userService
      .postDangKy(dataSignup)
      .then((res) => {
        handleSuccess();
        dispatch({
          type: GET_USER_INFO,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        handleFail();
      });
  };
};
