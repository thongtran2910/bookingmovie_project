import LayoutTheme from "../HOC/LayoutTheme";
import BookingHistoryPage from "../pages/BookingHistoryPage/BookingHistoryPage";
import BookingPage from "../pages/BookingPage/BookingPage";
import DetailPage from "../pages/DetailPage/DetailPage";
import HomePage from "../pages/HomePage/HomePage";
import ShowtimePage from "../pages/ShowtimePage/ShowtimePage";
import StartedPage from "../pages/StartedPage/StartedPage";
import UserInfo from "../pages/UserInfo/UserInfo";

export const userRoute = [
  {
    path: "/",
    component: <LayoutTheme Component={HomePage} />,
    exact: true,
    isUseLayout: true,
  },
  {
    path: "/detail/:id",
    component: <LayoutTheme Component={DetailPage} />,
    isUseLayout: true,
  },
  {
    path: "/showtime",
    component: <LayoutTheme Component={ShowtimePage} />,
    isUseLayout: true,
  },
  {
    path: "/booking/:bookingId",
    component: <LayoutTheme Component={BookingPage} />,
    isUseLayout: true,
  },
  {
    path: "/login",
    component: <LayoutTheme Component={StartedPage} />,
    isUseLayout: true,
  },
  {
    path: "/user",
    component: <LayoutTheme Component={UserInfo} />,
    isUseLayout: true,
  },
  {
    path: "/history",
    component: <LayoutTheme Component={BookingHistoryPage} />,
    isUseLayout: true,
  },
];
