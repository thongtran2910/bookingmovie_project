import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { userRoute } from "./routes/userRoute";

function App() {
  return (
    <div className="scroll-smooth">
      <BrowserRouter>
        <Switch>
          {userRoute.map((route, index) => {
            if (route.isUseLayout) {
              return (
                <Route
                  key={index}
                  exact={route.exact}
                  path={route.path}
                  render={() => {
                    return route.component;
                  }}
                />
              );
            }
            return (
              <Route
                key={index}
                exact={route.exact}
                path={route.path}
                component={route.component}
              />
            );
          })}
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
