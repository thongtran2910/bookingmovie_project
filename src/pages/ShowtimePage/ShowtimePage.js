import React from "react";
import MovieTabs from "./MovieTabs/MovieTabs";

export default function ShowtimePage() {
  return (
    <div className="bg-stone-100 h-screen">
      <div className="relative top-28 px-40">
        <h3 className="my-5 font-bold text-xl md:text-2xl xl:text-3xl text-gray-700 text-center">
          LỊCH CHIẾU
        </h3>
        <MovieTabs />
      </div>
    </div>
  );
}
