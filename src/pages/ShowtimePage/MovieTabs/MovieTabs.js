import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieService } from "../../../services/movieService";
import MovieTabItem from "./MovieTabItem";
const { TabPane } = Tabs;

const onChange = (key) => {
  console.log(key);
};

export default function MovieTabs() {
  const [movieTabs, setMovieTabs] = useState([]);
  useEffect(() => {
    let fetchMovieTabs = async () => {
      let result = await movieService.getMovieByTheater();
      setMovieTabs(result.data.content);
    };
    fetchMovieTabs();
  }, []);

  let renderContent = () => {
    return movieTabs.map((heThongRap, index) => {
      return (
        <TabPane
          tab={<img src={heThongRap.logo} className="w-10 h-10" />}
          key={index}
        >
          <Tabs
            style={{ height: "500" }}
            className="w-full"
            tabPosition="left"
            defaultActiveKey="1"
          >
            {heThongRap.lstCumRap.map((cumRap, index) => {
              if (index < 4) {
                return (
                  <TabPane
                    tab={
                      <div className="w-48 whitespace-normal text-left">
                        <p className="text-green-700 uppercase font-medium">
                          {cumRap.tenCumRap}
                        </p>
                        <p className="text-gray-700">{cumRap.diaChi}</p>
                      </div>
                    }
                    key={index}
                  >
                    <div className="overflow-auto h-[450px]">
                      {cumRap.danhSachPhim.map((movie, index) => {
                        if (index < 20) {
                          return <MovieTabItem key={index} movie={movie} />;
                        }
                      })}
                    </div>
                  </TabPane>
                );
              }
            })}
          </Tabs>
        </TabPane>
      );
    });
  };

  return (
    <Tabs
      style={{ height: 500 }}
      tabPosition="left"
      defaultActiveKey="1"
      onChange={onChange}
    >
      {renderContent()}
    </Tabs>
  );
}
