import React from "react";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";
import moment from "moment";
import { Tabs } from "antd";

export default function MovieTabItem({ movie }) {
  let isLogin = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  return (
    <div className="flex p-5 border-b border-gray-600 space-x-5 h-1/2 overflow-auto">
      <img src={movie.hinhAnh} alt="" className="w-32" />
      <div>
        <p className="font-medium text-xl mb-2">{movie.tenPhim}</p>
        <div class="grid grid-rows-2 grid-cols-2 gap-4 w-max">
          {movie.lstLichChieuTheoPhim.map((item) => {
            return (
              <NavLink
                to={isLogin == null ? "/login" : `/booking/${item.maLichChieu}`}
                className="border-solid border-2 p-2 rounded bg-gray-200 text-violet-800 font-medium hover:text-violet-500"
              >
                {moment(item.ngayChieuGioChieu).format("lll")}
              </NavLink>
            );
          })}
        </div>
      </div>
    </div>
  );
}
