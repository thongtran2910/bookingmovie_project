import moment from "moment";
import React, { useEffect, useState } from "react";
import { userService } from "../../services/userService";

export default function BookingHistoryPage() {
  const [ticketInfo, setTicketInfo] = useState({});

  useEffect(() => {
    userService
      .postUserInfo()
      .then((res) => {
        setTicketInfo(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="bg-stone-100 h-screen">
      <div className="relative top-24 container mx-auto">
        <div className="my-5 font-bold text-xl md:text-2xl xl:text-3xl text-gray-700 text-center uppercase">
          Thông tin vé đã đặt
        </div>
        <div className="h-184 overflow-auto">
          {ticketInfo.thongTinDatVe?.map((info, index) => {
            return (
              <div
                className="w-full flex mt-4 mb-8 border-b border-slate-500 pb-8"
                key={index}
              >
                <div className="w-1/2 lg:w-1/3 ">
                  <img src={info.hinhAnh} className="w-3/4 mx-auto my-2" />
                  <div className="w-3/4 mx-auto">
                    <p className="text-sm sm:text-lg text-red-500 font-medium">
                      {info.tenPhim.toLocaleUpperCase()}
                    </p>
                    <p>
                      <span className="font-medium">Thời lượng:</span>{" "}
                      {info.thoiLuongPhim}p
                    </p>
                  </div>
                </div>
                <div className="w-1/2 lg:w-2/3">
                  <p className="italic text-sm mb-2">
                    <span className="font-medium ">Đã đặt ngày:</span>{" "}
                    {moment(info.ngayDat).format("DD-MM-YYYY")} -{" "}
                    {moment(info.ngayDat).format("LT")}
                  </p>
                  <p className="text-sm sm:text-lg text-red-500 font-medium">
                    {info.danhSachGhe[0].tenHeThongRap.toLocaleUpperCase()}
                  </p>
                  <p className="text-sm sm:text-lg font-medium">
                    {info.danhSachGhe[0].maCumRap}
                  </p>
                  <p className="font-medium text-sm sm:text-lg">Tên ghế:</p>
                  <div className="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-8 gap-4 my-3">
                    {info.danhSachGhe.map((soGhe, index) => {
                      return (
                        <button
                          className="w-14 py-2 bg-green-200 rounded-md"
                          key={index}
                        >
                          {soGhe.tenGhe}
                        </button>
                      );
                    })}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}
