import { Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../../services/movieService";
import MovieSchedule from "./MovieSchedule";
const { TabPane } = Tabs;

export default function ThearterList() {
  const [cinema, setCinema] = useState({});
  let { id } = useParams();
  useEffect(() => {
    let fetchCinemaList = async () => {
      let result = await movieService.getMovieDetail(id);
      setCinema(result.data.content);
    };
    fetchCinemaList();
  }, []);
  const renderBookingTabs = () => {
    return cinema.heThongRapChieu?.map((heThongRap, index) => {
      return (
        <TabPane
          tab={<img src={heThongRap.logo} className="w-10" />}
          key={index}
        >
          <Tabs defaultActiveKey="1" tabPosition="left">
            {heThongRap.cumRapChieu.map((cumRap, index) => {
              return (
                <TabPane
                  tab={
                    <div className="w-64 whitespace-normal text-left">
                      <p className="text-green-700 uppercase font-medium">
                        {cumRap.tenCumRap}
                      </p>
                      <p className="text-black">{cumRap.diaChi}</p>
                    </div>
                  }
                  key={index}
                >
                  <MovieSchedule schedule={cumRap.lichChieuPhim} />
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };
  return (
    <div className="mx-auto my-8">
      <Tabs defaultActiveKey="1" tabPosition="left">
        {renderBookingTabs()}
      </Tabs>
    </div>
  );
}
