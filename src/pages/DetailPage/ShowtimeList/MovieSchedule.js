import { Tabs } from "antd";
import moment from "moment";
import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
const { TabPane } = Tabs;

export default function MovieSchedule({ schedule }) {
  const days = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];
  const showtimeList = [];
  schedule.map((lichChieu) => {
    let date = new Date(lichChieu.ngayChieuGioChieu).toLocaleDateString();
    let isDuplicate = true;
    for (let index in showtimeList) {
      if (showtimeList[index].ngay == date) {
        showtimeList[index].gioChieu.push({
          time: lichChieu.ngayChieuGioChieu,
          maLichChieu: lichChieu.maLichChieu,
        });
        isDuplicate = false;
      }
    }
    {
      isDuplicate &&
        showtimeList.push({
          ngay: date,
          ngayChieu: lichChieu.ngayChieuGioChieu,
          gioChieu: [
            {
              time: lichChieu.ngayChieuGioChieu,
              maLichChieu: lichChieu.maLichChieu,
            },
          ],
        });
    }
  });
  let isLogin = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  return (
    <div>
      <Tabs className="w-full" defaultActiveKey="1">
        {showtimeList.map((lich, index) => {
          let day = new Date(lich.ngayChieu);
          let weekday = days[day.getDay()];
          if (index < 8) {
            return (
              <TabPane
                tab={
                  <div className="px-3">
                    <div className="text-center text-green-700 lg:text-lg font-medium">
                      {weekday}
                    </div>
                    <div className="text-center text-black">
                      {moment(lich.ngayChieu)?.format("DD-MM")}
                    </div>
                  </div>
                }
                key={index}
              >
                <div className="grid grid-cols-3 md:grid-cols-4 lg:grid-cols-6 gap-y-3">
                  {lich.gioChieu.map((gio, index) => {
                    return (
                      <NavLink
                        to={
                          isLogin == null
                            ? `/login`
                            : `/booking/${gio.maLichChieu}`
                        }
                        key={index}
                        className="text-black hover:text-black"
                      >
                        <button className="bg-slate-100 w-full text-center px-3 py-1 rounded-md font-medium hover:bg-slate-200 transition duration-300 text-red-500 mr-4">
                          {moment(gio.time).format("LT")}
                        </button>
                      </NavLink>
                    );
                  })}
                </div>
              </TabPane>
            );
          }
        })}
      </Tabs>
    </div>
  );
}
