import { Modal, Progress } from "antd";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../services/movieService";
import { Tabs } from "antd";
import moment from "moment";
import ThearterList from "./ShowtimeList/ThearterList";
import { useWindowSize } from "../../Hook/useWindowSize";
const { TabPane } = Tabs;

const onChange = (key) => {};

export default function DetailPage() {
  let { id } = useParams();
  let windowSize = useWindowSize();
  const [movie, setMovie] = useState({});
  const [isVisible, setIsVisible] = useState(false);
  const [modalWidth, setModalWidth] = useState(0);

  const handleModalWidth = () => {
    if (windowSize.width > 1024) {
      setModalWidth(0.7 * windowSize.width);
    } else if (windowSize.width < 640) {
      setModalWidth(0.85 * windowSize.width);
    }
  };

  useEffect(() => {
    let fetchMovieDetail = async () => {
      let result = await movieService.getMovieDetail(id);
      setMovie(result.data.content);
    };
    fetchMovieDetail();
  }, []);
  // const handleBookingList = () => {
  //   setIsVisible(true);
  // };
  return (
    <div className="container mx-auto relative top-[80px] space-y-20 mt-20">
      <h2 className="text-center text-2xl md:text-3xl xl:text-4xl 2xl:text-4xl font-bold uppercase text-gray-700">
        Thông tin phim
      </h2>
      <div className="sm:flex sm:items-center sm:space-x-5 space-y-10 mx-auto px-5">
        <div className="w-80 sm:w-1/2 lg:w-1/3 2xl:w-1/4">
          <img
            src={movie.hinhAnh}
            className="w-full mx-auto md:w-96 2xl:w-96 rounded"
            alt=""
          />
        </div>
        <div className="sm:w-1/2">
          <Progress
            className="mb-10"
            type="circle"
            percent={movie.danhGia * 10}
            strokeColor={{
              "0%": "#108ee9",
              "100%": "#87d068",
            }}
            width={100}
            strokeWidth={8}
            format={(number) => {
              return (
                <span className="text-lilac text-sm md:text-base xl:text-lg">
                  {" "}
                  {number / 10} điểm{" "}
                </span>
              );
            }}
          />
          <p className="font-bold text-xl md:text-2xl xl:text-3xl mb-5 text-lilac uppercase">
            {movie.tenPhim}
          </p>
          <p className="font-normal text-base md:text-lg text-gray-700 mb-5">
            Ngày khởi chiếu:{" "}
            <span className="text-red-500">
              {moment(movie.ngayKhoiChieu).format("DD-MM-YYYY")}
            </span>
          </p>
          <p className="font-normal text-base md:text-lg text-gray-700 mb-5">
            Tình trạng:{" "}
            <span className="text-red-500">
              {movie.dangChieu ? "Đang chiếu" : "Sắp chiếu"}
            </span>
          </p>
          <p className="font-normal text-base md:text-lg text-gray-700 mb-5">
            {movie.moTa}
          </p>

          <a
            onClick={() => {
              setIsVisible(true);
              handleModalWidth();
            }}
            className="bg-lilac px-4 py-2 text-lg md:text-xl xl:text-2xl rounded-md text-white hover:text-white hover:bg-violet-500 transition duration-200"
          >
            Đặt vé
          </a>
        </div>
      </div>
      <div>
        <Modal
          title="Lịch chiếu phim"
          onCancel={() => setIsVisible(false)}
          visible={isVisible}
          footer={false}
          width={1000}
          bodyStyle={{ overflow: "auto" }}
        >
          <ThearterList />
        </Modal>
      </div>
    </div>
  );
}
