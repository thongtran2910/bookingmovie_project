import { Card } from "antd";
import moment from "moment";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

const MovieItem = ({ movie }) => (
  <Card
    id="movie-list"
    hoverable
    className="w-[14rem] h-[25rem] sm:w-[20rem] sm:h-[33rem] md:w-[20rem] md:h-[36rem] lg:w-[20rem] lg:h-[36rem] xl:w-[19rem] relative"
    cover={
      <div>
        <img
          id="movie-pics"
          className="h-[17rem] w-full 2xl:w-full xl:w-full lg:w-full lg:h-[28rem] md:w-full md:h-[28rem] sm:w-full sm:h-[25rem]"
          alt="banner"
          src={movie.hinhAnh}
        />
        <div className="overlay">
          <NavLink
            to={`detail/${movie.maPhim}`}
            className="detail-btn text-center text-sm sm:text-lg uppercase bg-lilac hover:bg-violet-900 px-5 py-2 rounded text-white hover:text-white mt-3"
          >
            Chi tiết
          </NavLink>
        </div>
      </div>
    }
  >
    <div id="card-body">
      <Meta
        title={movie.tenPhim}
        description={
          <span className="text-lg font-medium text-violet-400">
            {moment(movie.ngayKhoiChieu).format("DD/MM/YYYY")}
          </span>
        }
      />
      <p className="truncate text-gray-500 font-medium mt-2">{movie.moTa}</p>
    </div>
  </Card>
);

export default MovieItem;
