import React from "react";

export default function footer() {
  return (
    <div className="max-h-max">
      <div
        className="h-full m-auto pt-8 md:w-3/4 sm:w-full "
        style={{ maxWidth: 1200 }}
      >
        <table
          className="table-auto w-full border-b-2 border-slate-400 "
          style={{}}
        >
          <thead>
            <tr className="text-start text-gray-400">
              <th>BOOKING TICKET</th>
              <th>ĐỐI TÁC</th>
              <th>MOBILE APP</th>
              <th>SOCIAL</th>
            </tr>
          </thead>
          <tbody>
            <tr className="">
              <td className="align-top">
                <div className="grid py-8 lg:pl-8 pl-2 lg:grid-cols-2 pr-2 text-sm grid-cols-1">
                  <p className="hover:text-white text-gray-400 transition duration-300 cursor-pointer">
                    FAQ
                  </p>
                  <p className="hover:text-white text-gray-400 transition duration-300 cursor-pointer">
                    Thỏa thuận sử dụng
                  </p>
                  <p className="hover:text-white text-gray-400 transition duration-300 cursor-pointer">
                    Brand Guidelines
                  </p>
                  <p className="hover:text-white text-gray-400 transition duration-300 cursor-pointer">
                    Chính sách bảo mật
                  </p>
                </div>
              </td>
              <td className="align-top">
                <div className="grid md:grid-cols-3 py-8 grid-cols-2 justify-items-center">
                  <div
                    className="h-10 w-10 small_wh   rounded-full  mb-4"
                    style={{
                      backgroundImage:
                        "url(https://gigamall.com.vn/data/2019/05/06/11365490_logo-cgv-500x500.jpg)",
                      backgroundSize: "cover",
                    }}
                  >
                    <a
                      href="https://www.cgv.vn/"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                  <div
                    className="h-10 w-10 small_wh   rounded-full  mb-4"
                    style={{
                      backgroundImage:
                        "url(https://www.bhdstar.vn/wp-content/uploads/2019/06/BHDStar_Logo_Tron.png)",
                      backgroundSize: "cover",
                    }}
                  >
                    <a
                      href="https://www.bhdstar.vn/"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                  <div
                    className="h-10 w-10 small_wh   rounded-full  mb-4"
                    style={{
                      backgroundImage:
                        "url(https://nextphim.com/wp-content/uploads/2018/06/logo-galaxy-cinema.jpg)",
                      backgroundSize: "cover",
                    }}
                  >
                    <a
                      href="https://www.galaxycine.vn/"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                  <div
                    className="h-10 w-10 small_wh   rounded-full  mb-4"
                    style={{
                      backgroundImage:
                        "url(https://tenpack.com.vn/wp-content/uploads/2016/02/cinestar-logo.png)",
                      backgroundSize: "cover",
                    }}
                  >
                    <a
                      href="http://cinestar.com.vn/"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                  <div
                    className="h-10 w-10 small_wh   rounded-full  mb-4"
                    style={{
                      backgroundImage:
                        "url(https://cdn.nhanlucnganhluat.vn/uploads/images/D69545BE/logo/2019-04/pictures_library_6235_20180102135750_4563.jpg)",
                      backgroundSize: "cover",
                    }}
                  >
                    <a
                      href="https://lottecinemavn.com"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                  <div
                    className="h-10 w-10 small_wh   rounded-full  mb-4"
                    style={{
                      backgroundImage:
                        "url(https://scontent.fsgn5-11.fna.fbcdn.net/v/t1.18169-9/11738012_638376569630533_4842848653150639340_n.png?_nc_cat=103&ccb=1-7&_nc_sid=09cbfe&_nc_ohc=a-gnlfDai58AX-r43PL&_nc_ht=scontent.fsgn5-11.fna&oh=00_AT9eA5au6SF2LT-6hXHxjojf3pbneuUQkScsvU_75lUq2Q&oe=62DAD212)",
                      backgroundSize: "cover",
                    }}
                  >
                    <a
                      href="https://www.megagscinemas.vn/"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                  <div
                    className="h-10 w-10 small_wh   rounded-full  mb-4 bg-white"
                    style={{
                      backgroundImage:
                        "url(https://truyenthongvang.vn/uploads/public/2021/06/03/1622682588188_zalopay.png)",
                      backgroundSize: "cover",
                    }}
                  >
                    <a
                      href="https://zalopay.vn/"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                  <div
                    className="h-10 w-10 small_wh   rounded-full  mb-4"
                    style={{
                      backgroundImage:
                        "url(https://www.inlogo.vn/vnt_upload/File/Image/logo_VCB_828891.jpg)",
                      backgroundSize: "cover",
                      backgroundPosition: " -5px 0",
                    }}
                  >
                    <a
                      href="https://www.vietcombank.com.vn/"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                </div>
              </td>
              <td className="align-top">
                <div className="grid grid-cols-1 py-8 justify-items-center">
                  <div
                    className="h-10 w-10 small_wh mb-4 rounded-full"
                    style={{
                      backgroundImage:
                        "url(https://logos-world.net/wp-content/uploads/2021/02/App-Store-Logo.png)",
                      backgroundSize: "cover",
                      backgroundPosition: " center center ",
                    }}
                  >
                    <a
                      href="https://www.apple.com/vn/app-store/"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                  <div
                    className="h-10 w-10 small_wh   rounded-full bg-white"
                    style={{
                      backgroundImage:
                        "url(https://anhnbt.com/apps/wp-content/uploads/2018/05/google-play-store-ico.png)",
                      backgroundSize: "cover",
                      backgroundPosition: " 2px center",
                    }}
                  >
                    <a
                      href="https://play.google.com/"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                </div>
              </td>
              <td className="align-top">
                <div className="grid grid-cols-1 py-8  justify-items-center">
                  <div
                    className="h-10 w-10 small_wh  rounded-full mb-4"
                    style={{
                      backgroundImage:
                        "url(https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Facebook_Logo_%282019%29.png/800px-Facebook_Logo_%282019%29.png)",
                      backgroundSize: "cover",
                      backgroundPosition: " 0 0",
                    }}
                  >
                    <a
                      href="https://www.facebook.com"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                  <div
                    className="h-10 w-10 small_wh   rounded-full bg-white"
                    style={{
                      backgroundImage:
                        "url(https://truyenthongvang.vn/uploads/public/2021/06/03/1622682588188_zalopay.png)",
                      backgroundSize: "cover",
                      backgroundPosition: " 0 0",
                    }}
                  >
                    <a
                      href="https://zalo.me/pc"
                      className="hover:text-white cursor-pointer h-full w-full inline-block"
                      style={{
                        color: "#9e9e9e",
                      }}
                    ></a>
                  </div>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
        <div className="border-t-1">
          <p className="text-gray-400 text-center text-sm py-4">
            Copyright © 2022 BookingTicket. All Rights Reserved
          </p>
        </div>
      </div>
    </div>
  );
}
