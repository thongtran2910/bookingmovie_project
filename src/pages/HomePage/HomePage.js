import _ from "lodash";
import React, { useEffect, useState } from "react";
import { movieService } from "../../services/movieService";
import MovieCarousel from "./MovieCarousel/MovieCarousel";
import "./homePage.css";
import MovieBanner from "./MovieBanner/MovieBanner";
import Footer from "./Footer/Footer";

export default function HomePage() {
  const [movieList, setMovieList] = useState([]);
  const [movieBanner, setMovieBanner] = useState([]);

  useEffect(() => {
    let fetchMovieList = async () => {
      let result_1 = await movieService.getMovieList();
      let chunkedList = _.chunk(result_1.data.content, 8);
      setMovieList(chunkedList);
    };
    let fetchMovieBanner = async () => {
      let result_2 = await movieService.getMovieBanner();
      setMovieBanner(result_2.data.content);
    };
    fetchMovieList();
    fetchMovieBanner();
  }, []);
  return (
    <div>
      <div id="movie-banner" className="mt-24">
        <MovieBanner bannerList={movieBanner} />
      </div>
      <div className="bg-stone-200 pb-10">
        <div id="carousel_homepage" className="max-h-max container mx-auto">
          <MovieCarousel chunkedList={movieList} />
        </div>
      </div>
      <div className="bg-slate-800">
        <div className="max-h-max container mx-auto">
          <Footer />
        </div>
      </div>
    </div>
  );
}
