import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/effect-fade";
import { Autoplay, Navigation, EffectFade } from "swiper";
import "../MovieBanner/swiperBanner.css";

export default function MovieBanner({ bannerList }) {
  return (
    <Swiper
      navigation={true}
      autoplay={{
        delay: 3000,
        disableOnInteraction: false,
      }}
      loop={true}
      effect={"fade"}
      spaceBetween={50}
      slidesPerView={1}
      modules={[Navigation, Autoplay, EffectFade]}
    >
      {bannerList.map((movie, index) => {
        return (
          <SwiperSlide>
            <img
              className="banner-pics max-h-[800px] max-w-screen 2xl:w-screen xl:w-screen lg:w-screen md:w-screen sm:w-screen"
              src={movie.hinhAnh}
              alt="banner"
            />
          </SwiperSlide>
        );
      })}
    </Swiper>
  );
}
