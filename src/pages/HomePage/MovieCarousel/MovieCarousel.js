import MovieItem from "../MovieItem/MovieItem";
import "swiper/css";
import "swiper/css/pagination";
import { Carousel } from "antd";

const MovieCarousel = ({ chunkedList }) => {
  const onChange = (currentSlide) => {};

  return (
    <div className="h-max">
      <div className="border-b-4 border-gray-600 mb-10 text-center w-1/2 mx-auto sm:w-1/3 2xl:w-1/4">
        <h3 className="uppercase pt-10 text-gray-600 text-lg sm:text-xl md:text-2xl 2xl:text-3xl">
          Danh sách phim
        </h3>
      </div>
      <Carousel draggable={true} afterChange={onChange}>
        {chunkedList.map((movies, index) => {
          return (
            <div className="mb-10">
              <div className="grid grid-cols-1 justify-items-center gap-10 xl:grid-cols-3 xl:gap-10 2xl:gap-10 2xl:grid-cols-4 lg:grid-cols-2 lg:gap-10 md:grid-cols-2 md:gap-8 sm:grid-cols-1 sm:gap-10">
                {movies.map((item) => {
                  return <MovieItem movie={item} />;
                })}
              </div>
            </div>
          );
        })}
      </Carousel>
    </div>
  );
};

export default MovieCarousel;
