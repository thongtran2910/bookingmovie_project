import { Modal } from "antd";
import { message } from "antd";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { movieService } from "../../services/movieService";
import Lottie from "lottie-react";
import tickedAnimate from "../../assets/success-animate.json";
import moment from "moment";

export default function BookingPage() {
  // Modal
  const [visible, setVisible] = useState(false);
  // Booking feature
  let { bookingId } = useParams();
  let bgColor = "";
  let alphabet = "ABCDEFGHIJ";

  const [movie, setMovie] = useState({});
  const [seat, setSeat] = useState([]);
  const [chosenSeatInfor, setChosenSeatInfor] = useState([]);
  const [chosenSeatList, setChosenSeatList] = useState([]);
  class ChosenSeat {
    constructor(seatName, price) {
      this.seatName = seatName;
      this.price = price;
    }
  }
  let totalPaid = chosenSeatList.reduce((Acc, curr) => {
    return (Acc += curr.price);
  }, 0);
  useEffect(() => {
    let fetchBookingLayout = async () => {
      let result = await movieService.getBookingTheaterDetail(bookingId);
      let chunkedData = _.chunk(result.data.content.danhSachGhe, 16);
      setMovie(result.data.content);
      setSeat(chunkedData);
    };
    fetchBookingLayout();
  }, []);

  const handleChooseSeat = (indexSeat, indexRow, seat, el) => {
    let idSeat = indexSeat + 1;

    let tenSoGhe = alphabet[indexRow] + idSeat;
    let chosenSeat = new ChosenSeat(tenSoGhe, seat.giaVe);
    setChosenSeatInfor((prev) => {
      let isChosen = false;
      for (let index in prev) {
        let checkDup = JSON.stringify(prev[index]) === JSON.stringify(seat);

        if (checkDup) {
          isChosen = true;
          break;
        } else {
          isChosen = false;
        }
      }

      if (isChosen) {
        return chosenSeatInfor.filter((item) => {
          return item.maGhe !== seat.maGhe;
        });
      } else {
        let clonePrev = [...prev];

        clonePrev.push(seat);
        return clonePrev;
      }
    });
    setChosenSeatList((prev) => {
      let isChosen = false;
      for (let index in prev) {
        let checkDup =
          JSON.stringify(prev[index]) === JSON.stringify(chosenSeat);

        if (checkDup) {
          isChosen = true;
          break;
        } else {
          isChosen = false;
        }
      }

      if (isChosen) {
        el.target.classList.remove("!bg-red-900");
        return chosenSeatList.filter((item) => {
          return item.seatName !== chosenSeat.seatName;
        });
      } else {
        el.target.classList.add("!bg-red-900");
        let clonePrev = [...prev];
        clonePrev.push(chosenSeat);
        return clonePrev;
      }
    });
  };
  let history = useHistory();
  const handlePurchase = () => {
    if (chosenSeatInfor.lenght !== 0) {
      movieService
        .postBookingTicket({
          maLichChieu: bookingId,
          danhSachVe: chosenSeatInfor,
        })
        .then((res) => {
          console.log(res.data.content);
          if (res.data.content === "Đặt vé thành công!") {
            setVisible(true);
            setTimeout(() => {
              history.push("/history");
            }, 2000);
          }
        })
        .catch((err) => {
          message.error(err.data.content);
        });
    }
  };

  return (
    <div className="bg-slate-900 overflow-y-auto lg:overflow-auto w-full h-screen">
      <div className="relative top-[100px] py-12 m-auto max-w-[1200px]">
        <div className="grid grid-cols-1 m-auto w-full lg:grid-cols-2 items-center justify-center space-y-10 justify-items-center">
          {/* màn hình */}
          <div className="w-full sm:w-5/6 md:w-4/5 lg:w-fit sm:overflow-auto flex flex-col rounded-xl">
            <div className="h-8 w-full bg-slate-600 text-center text-lg text-white">
              Màn Hình
            </div>
            {/* Ghế */}
            <div className="w-full my-4 flex justify-center">
              <table className="table-auto m-auto black max-w-max">
                <tbody>
                  {seat.map((row, indexRow) => {
                    return (
                      <tr key={indexRow}>
                        <td className="w-8">
                          <div className="text-sm md:text-base text-white">
                            {alphabet[indexRow]}
                          </div>
                        </td>
                        <td>
                          <div className="flex">
                            {row.map((seat, indexSeat) => {
                              if (seat.loaiGhe === "Thuong") {
                                bgColor = "bg-stone-600 hover:bg-stone-400";
                              } else {
                                bgColor = "bg-red-500 hover:bg-rose-300";
                              }
                              return seat.daDat ? (
                                <button
                                  key={indexSeat}
                                  className="w-4 h-4 md:h-5 md:w-5 lg:h-6 lg:w-6 xl:h-7 xl:w-7 bg-stone-200 my-1 mx-1 rounded-sm md:rounded-md font-medium sm:text-xs md:text-sm"
                                >
                                  <span>X</span>
                                </button>
                              ) : (
                                <button
                                  className={`w-4 h-4 md:h-5 md:w-5 lg:h-6 lg:w-6 xl:h-7 xl:w-7 text-white ${bgColor} mx-1 my-1 md:rounded-md rounded-sm transition duration-200`}
                                  key={indexSeat}
                                  onClick={(el) => {
                                    handleChooseSeat(
                                      indexSeat,
                                      indexRow,
                                      seat,
                                      el
                                    );
                                  }}
                                >
                                  {seat.tenGhe}
                                </button>
                              );
                            })}
                          </div>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            {/* Notes */}
            <div className="grid grid-cols-2 text-white">
              <div className="flex items-center  mr-3 ">
                <button
                  className={`w-4 h-4  bg-stone-600 mx-1 my-1 rounded-md`}
                ></button>
                <span className="text-sm">: Ghế thường</span>
              </div>
              <div className="flex items-center  mr-3">
                <button
                  className={`w-4 h-4  bg-red-900 mx-1 my-1 rounded-md`}
                ></button>
                <span className="text-sm">: Ghế đang chọn</span>
              </div>
              <div className="flex items-center  mr-3">
                <button
                  className={`w-4 h-4 leading-3 bg-stone-200 mx-1 my-1 rounded-md font-medium`}
                >
                  <span className="text-black">X</span>
                </button>
                <span className="text-sm">: Ghế đã đặt</span>
              </div>
              <div className="flex items-center  mr-3">
                <button
                  className={`w-4 h-4  bg-red-500 mx-1 my-1 rounded-md`}
                ></button>
                <span className="text-sm">: Ghế VIP</span>
              </div>
            </div>
          </div>
          {/* đặt vé */}
          <div className="w-5/6 md:my-7 md:w-4/5 xl:w-4/5 border-solid border-2 border-black bg-stone-300 rounded-lg shadow-xl px-5 pb-4">
            <h2 className="text-center text-lg lg:text-2xl 2xl:text-3xl text-lilac font-bold border border-t-0 border-l-0 border-r-0 border-solid py-5">
              {new Intl.NumberFormat("vi-VI").format(totalPaid)} VND
            </h2>
            <div className="flex justify-between border border-t-0 border-l-0 border-r-0 text-base lg:text-xl py-5">
              <p className="font-medium">Cụm rạp:</p>
              <p className="text-lilac font-medium">
                {movie.thongTinPhim?.tenCumRap}
              </p>
            </div>
            <div className="flex justify-between border border-t-0 border-l-0 border-r-0 text-base lg:text-xl py-5">
              <p className="font-medium">Địa chỉ:</p>
              <p className="text-lilac font-medium">
                {movie.thongTinPhim?.diaChi}
              </p>
            </div>
            <div className="flex justify-between border border-t-0 border-l-0 border-r-0 text-base lg:text-xl py-5">
              <p className="font-medium">Rạp:</p>
              <p className="text-lilac font-medium">
                {movie.thongTinPhim?.tenRap}
              </p>
            </div>
            <div className="flex justify-between border border-t-0 border-l-0 border-r-0 text-base lg:text-xl py-5">
              <p className="font-medium">Ngày giờ chiếu:</p>
              <p className="text-lilac font-medium">
                {moment(movie.thongTinPhim?.ngayChieu).format("ll")}~
                <span className="text-rose-600">
                  {movie.thongTinPhim?.gioChieu}
                </span>
              </p>
            </div>
            <div className="flex justify-between border border-t-0 border-l-0 border-r-0 text-base lg:text-xl py-5">
              <p className="font-medium">Tên phim:</p>
              <p className="text-lilac font-medium">
                {movie.thongTinPhim?.tenPhim}
              </p>
            </div>
            <div className="flex justify-between border border-t-0 border-l-0 border-r-0 text-base lg:text-xl py-5 mb-5">
              <p className="font-medium">Ghế đã chọn:</p>
              <span>
                {chosenSeatList.map((thongTinGhe, index) => {
                  return (
                    <span key={index} className="font-medium text-lilac">
                      {thongTinGhe.seatName},{" "}
                    </span>
                  );
                })}
              </span>
            </div>
            <div className="text-center">
              <button
                onClick={() => {
                  handlePurchase();
                  setVisible(true);
                }}
                className="px-4 py-3 my-5 w-1/3 text-xl bg-lilac hover:bg-violet-900 duration-200 text-white rounded-lg"
              >
                Đặt vé
              </button>

              <Modal
                title={false}
                footer={false}
                closable={false}
                visible={visible}
              >
                <p className="uppercase text-2xl text-lilac font-bold text-center">
                  Đặt vé thành công
                </p>
                <Lottie
                  animationData={tickedAnimate}
                  className="w-1/2 mx-auto"
                />
              </Modal>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
