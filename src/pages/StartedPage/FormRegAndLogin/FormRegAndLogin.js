import { Button, Form, Input, message, Modal } from "antd";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  getUserInfoActionService,
  setUserInfoActionService,
} from "../../../redux/actions/userAction";
import "./FormRegAndLogin.css";
import Lottie from "lottie-react";
import tickedAnimate from "../../../assets/success-animate.json";

export default function FormRegAndLogin() {
  const [style, setStyle] = useState("user_options-forms bounceRight");
  const handleChangeStyle = () => {
    setStyle("user_options-forms bounceLeft");
  };
  const handleLoginBtn = () => {
    setStyle("user_options-forms bounceRight");
  };
  // Đăng nhập
  let history = useHistory();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    let onSuccess = () => {
      setVisibleSignIn(true);
      setTimeout(() => {
        history.push("/");
      }, 2000);
    };
    let onFail = () => {
      message.error("Tài khoản hoặc mật khẩu không chính xác");
    };

    dispatch(setUserInfoActionService(values, onSuccess, onFail));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  // Đăng ký

  const [form] = Form.useForm();

  const onSubmit = (values) => {
    let onSuccess = () => {
      setVisibleSignUp(true);
      setTimeout(() => {
        setVisibleSignUp(false);
      }, 2000);
    };
    let onFail = () => {
      message.error("Vui lòng kiểm tra lại thông tin của bạn!");
    };
    dispatch(getUserInfoActionService(values, onSuccess, onFail));
  };
  const onSubmitFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  // Modal
  const [visibleSignUp, setVisibleSignUp] = useState(false);
  const [visibleSignIn, setVisibleSignIn] = useState(false);
  return (
    <section className="user">
      <div className="user_options-container">
        <div className="user_options-text">
          <div className="user_options-unregistered">
            <h2 className="user_unregistered-title">Bạn chưa có tài khoản?</h2>
            <p className="user_unregistered-text">
              Nhấn nút đăng ký bên dưới để đăng ký nhé
            </p>
            <button
              onClick={handleChangeStyle}
              className="user_unregistered-signup"
              id="signup-button"
            >
              Đăng ký
            </button>
          </div>
          <div className="user_options-registered">
            <h2 className="user_registered-title">Bạn đã có tài khoản?</h2>
            <p className="user_registered-text">
              Nhanh chóng đăng nhập để đặt vé nào
            </p>
            <button
              onClick={handleLoginBtn}
              className="user_registered-login"
              id="login-button"
            >
              Đăng nhập
            </button>
          </div>
        </div>
        <div className={style} id="user_options-forms">
          <div className="user_forms-login">
            <h2 className="forms_title">Đăng nhập</h2>
            <Form
              className="forms_form"
              name="basic"
              layout="vertical"
              labelCol={{
                span: 8,
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <fieldset className="forms_fieldset">
                <div className="forms_field">
                  <Form.Item
                    name="taiKhoan"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập tên tài khoản của bạn!",
                      },
                    ]}
                  >
                    <Input placeholder="Tài khoản" />
                  </Form.Item>
                </div>
                <div className="forms_field">
                  <Form.Item
                    name="matKhau"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập mật khẩu của bạn!",
                      },
                    ]}
                  >
                    <Input.Password placeholder="Mật khẩu" />
                  </Form.Item>
                </div>
              </fieldset>
              <div className="forms_buttons">
                <button type="button" className="forms_buttons-forgot">
                  Quên mật khẩu?
                </button>
                <Button
                  className="forms_buttons-action"
                  type="primary"
                  htmlType="submit"
                >
                  Đăng nhập
                </Button>
              </div>
            </Form>
          </div>
          <div className="user_forms-signup">
            <h2 className="forms_title">Đăng ký</h2>
            <Form
              className="forms_form"
              form={form}
              name="register"
              labelCol={{
                span: 6,
              }}
              onFinish={onSubmit}
              onFinishFailed={onSubmitFailed}
              scrollToFirstError
            >
              <fieldset className="forms_fieldset">
                <div className="forms_field">
                  <Form.Item
                    name="hoTen"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập tên họ tên của bạn!",
                        whitespace: true,
                      },
                    ]}
                  >
                    <Input placeholder="Họ tên" />
                  </Form.Item>
                </div>
                <div className="forms_field">
                  <Form.Item
                    name="taiKhoan"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập tên tài khoản của bạn!",
                        whitespace: true,
                      },
                    ]}
                  >
                    <Input placeholder="Tài khoản" />
                  </Form.Item>
                </div>
                <div className="forms_field">
                  <Form.Item
                    name="matKhau"
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập mật khẩu của bạn!",
                      },
                    ]}
                    hasFeedback
                  >
                    <Input.Password placeholder="Mật khẩu" />
                  </Form.Item>
                </div>
                <div className="forms_field">
                  <Form.Item
                    name="confirm"
                    dependencies={["matKhau"]}
                    hasFeedback
                    rules={[
                      {
                        required: true,
                        message: "Vui lòng nhập lại mật khẩu của bạn!",
                      },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue("matKhau") === value) {
                            return Promise.resolve();
                          }

                          return Promise.reject(
                            new Error("Mật khẩu bạn đã nhập không trùng nhau!")
                          );
                        },
                      }),
                    ]}
                  >
                    <Input.Password placeholder="Xác nhận mặt khẩu" />
                  </Form.Item>
                </div>
                <div className="forms_field">
                  <Form.Item
                    name="email"
                    rules={[
                      {
                        type: "email",
                        message: "Email không hợp lệ!",
                      },
                      {
                        required: true,
                        message: "Vui lòng nhập Email của bạn!",
                      },
                    ]}
                  >
                    <Input placeholder="Email" />
                  </Form.Item>
                </div>
              </fieldset>
              <div className="forms_buttons">
                <Button
                  className="forms_buttons-action"
                  type="primary"
                  htmlType="submit"
                >
                  Đăng ký
                </Button>
              </div>
            </Form>
          </div>
        </div>
        <Modal
          title={false}
          footer={false}
          closable={false}
          visible={visibleSignUp}
        >
          <p className="uppercase text-2xl text-lilac font-bold text-center">
            Đăng ký thành công
          </p>
          <Lottie animationData={tickedAnimate} className="w-1/2 mx-auto" />
        </Modal>
        <Modal
          title={false}
          footer={false}
          closable={false}
          visible={visibleSignIn}
        >
          <p className="uppercase text-2xl text-lilac font-bold text-center">
            Đăng nhập thành công
          </p>
          <Lottie animationData={tickedAnimate} className="w-1/2 mx-auto" />
        </Modal>
      </div>
    </section>
  );
}
