import { Modal } from "antd";
import React, { useEffect, useState } from "react";
import { userService } from "../../services/userService";
import UpdateUserInfo from "./UpdateUserInfo";

export default function UserInfo() {
  const [userInfor, setUserInfor] = useState({});
  const [modalVisible, setModalVisible] = useState(false);
  const [userInforChange, setUserInforChange] = useState(0);

  useEffect(() => {
    userService
      .postUserInfo()
      .then((res) => {
        setUserInfor(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [userInforChange]);

  return (
    <div className="w-80 md:w-184 sm:w-128 lg:w-240 mx-auto my-8 relative top-[100px]">
      <div className=" " style={{ backgroundColor: "#EFEFEF" }}>
        <table className="table-auto w-5/6 font-medium mx-auto text-sm xs:text-base">
          <tbody className="w-full">
            <tr className="w-full h-12 border-collapse border-b border-gray-300">
              <td className="w-1/2">Họ tên:</td>
              <td className="text-right ">{userInfor.hoTen}</td>
            </tr>
            <tr className="w-full h-12 border-collapse border-b border-gray-300">
              <td className="w-1/2">Email:</td>
              <td className="text-right ">{userInfor.email}</td>
            </tr>
            <tr className="w-full h-12 border-collapse border-b border-gray-300">
              <td className="w-1/2">Số điện thoại:</td>
              <td className="text-right ">{userInfor.soDT}</td>
            </tr>
            <tr className="w-full h-12 border-collapse border-b border-gray-300">
              <td className="w-1/2">Tài khoản:</td>
              <td className="text-right ">{userInfor.taiKhoan}</td>
            </tr>
            <tr className="w-full h-12 border-collapse border-b border-gray-300">
              <td className="w-1/2">Mật khẩu:</td>
              <td className="text-right">{userInfor.matKhau}</td>
            </tr>
          </tbody>
        </table>

        <div className="text-center">
          <button
            className="bg-blue-600 px-3 py-2 text-white rounded-md my-4"
            onClick={() => {
              setModalVisible(true);
            }}
          >
            Thay đổi thông tin
          </button>
          <Modal
            title={null}
            centered
            visible={modalVisible}
            onOk={() => {
              setModalVisible(false);
            }}
            onCancel={() => {
              setModalVisible(false);
            }}
            footer={null}
          >
            <UpdateUserInfo
              userInfor={userInfor}
              setUserInforChange={setUserInforChange}
              setModalVisible={setModalVisible}
            />
          </Modal>
        </div>
      </div>
    </div>
  );
}
